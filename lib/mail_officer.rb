class MailOfficer

	def self.send_validation_email(user_id)
		user = User.find(user_id)

		if (not user.nil?)
			user.send_activation_instructions!
		end
	end

	def self.send_welcoming_email(user_id)
		user = User.find(user_id)

		if (not user.nil?)
			user.send_welcoming_email!
		end
	end

	def self.user_not_using(user_id)
		user = User.find(user_id)

		if (not user.nil?)
			user.send_reminder_not_using!
		end
	end

	def self.user_not_validating(user_id)
		user = User.find(user_id)

		if (not user.nil?)
			user.send_reminder_not_validating!
		end
	end

	def self.user_missing_days(user_id)
		user = User.find(user_id)

		if (not user.nil?)
			user.send_reminder_missing_days!
		end
	end

	def self.user_missing_validation(user_id)
		user = User.find(user_id)

		if (not user.nil?)
			user.send_reminder_missing_validation!
		end
	end

	def self.user_not_validating_after_having_validated(user_id)
		user = User.find(user_id)

		if (not user.nil?)
			user.send_reminder_not_validating_after_validated!
		end
	end

	def self.user_not_using_after_start_using(user_id)
		user = User.find(user_id)

		if (not user.nil?)
			user.send_reminder_not_using_after_start_using!
		end
	end

	def self.ready_for_exit(user_id)
		user = User.find(user_id)

		if (not user.nil?)
			user.send_ready_for_exit!
		end
	end

	def self.mobile_app_off(user_id)
		user = User.find(user_id)
		if (not user.nil?)
			user.send_mobile_app_off!
		end
	end
end
