var smart;
if (smart == undefined){
	smart = {};
}

smart.RawDataViz = function(options){
	this.initialize(options);
}

smart.RawDataViz.prototype = {
	config: {
		MARGIN_LEFT:100,
		WIDTH: 450,
		HEIGHT: 50,
		TICKS_HEIGHT: 15,
		NUMBER_TICKS: 8
	},

	initialize: function(options){
		_.bindAll(this, "loadData", "configureGraph", "draw_data", "render", "unload", "draw_gps", "draw_existance", "draw_acce_force", "draw_battery");

		this.min_timestamp = 99999999999999999999;
		this.max_timestamp = 0;

		this.options = options;
		this.container = jQuery(options.el);
	},

	loadData: function(data){
		// console.log(data);

		// --- save stuff
		this.gps_data = data.gps;
		this.acce_data = data.acce;
		this.battery_data = data.battery;
		this.device = data.device

		this.min_timestamp = data.unix_start_time;
		this.max_timestamp = this.min_timestamp + 86400000;
		this.time_offset = data.time_offset;

		console.log("loading data with ", this.min_timestamp, this.max_timestamp);
		this.configureGraph();
	},

	configureGraph: function () {

		var format = d3.time.format("%X");

		this._scaleX = d3.time.scale()
			.domain([new Date(this.min_timestamp), new Date(this.max_timestamp)])
			.range([0, this.config.WIDTH]);

		window.cenas =this._scaleX;
	},


	draw_grid: function(ctx) {
		var max_y = this.config.HEIGHT-this.config.TICKS_HEIGHT;
		try {
		  /* vertical lines */
		  for (var x = 0.5; x < this.config.WIDTH; x += 5) {
			ctx.moveTo(x, 0);
			ctx.lineTo(x, max_y);
		  }

		  /* horizontal lines */
		  for (var y = 0.5; y < max_y; y += 5) {
			ctx.moveTo(0, y);
			ctx.lineTo(this.config.WIDTH, y);
		  }

		  /* draw it! */
		  ctx.strokeStyle = "#0d0d0d";
		  ctx.strokeStyle = "#ccc";
		  ctx.stroke();
		} catch(err) {

		}
	},

	draw_labels: function(context){
		var x;
		var markingb_y = this.config.HEIGHT-this.config.TICKS_HEIGHT;

		var ticks = this._scaleX.ticks(this.config.NUMBER_TICKS);
		context.beginPath();

		context.strokeStyle = "#000";
		context.fillStyle="#000";
		context.font = "bold 9px verdana";
		context.textBaseline = "bottom";
		context.textAlign = "center"

		context.moveTo(0, markingb_y);
		context.lineTo(this.config.WIDTH, markingb_y);

		_.each(ticks, function(elm, index){
			var ts = elm.getTime();
			x = this._scaleX(ts);

			context.moveTo(x, markingb_y);
			context.lineTo(x, markingb_y+5);

			var label_text = moment(ts).zone(this.time_offset).format("HH:mm");
			context.fillText(label_text, x, this.config.HEIGHT);
		}, this);
		context.stroke();
	},

	draw_existance: function(context, data){
		if (!data || data.length === 0) return;

		context.beginPath();
		context.strokeStyle = "#A01517";
		var x;
		_.each(data, function(elm, index){
			x = this._scaleX(elm.timestamp*1000);
			context.moveTo(x, 0);
			context.lineTo(x, this.config.HEIGHT-this.config.TICKS_HEIGHT);
		}, this);

		context.stroke();
	},

	draw_gps: function(context, data){
		if (!data || data.length === 0) return;

		var max_accuracy= _.max(data, function(elm){ return parseInt(elm.accuracy,10); }).accuracy;
		var min_accuracy= _.min(data, function(elm){ return parseInt(elm.accuracy,10); }).accuracy;

		var color_scale = d3.scale.linear()
							.domain([parseInt(max_accuracy,10)+100, 0])
							.rangeRound([250, 360]);

		var x;
		_.each(data, function(elm, index){
			context.beginPath();
			context.strokeStyle = "hsl("+color_scale(parseInt(elm.accuracy,10))+", 100%,50%)";
			x = this._scaleX(elm.timestamp*1000);
			context.moveTo(x, 0);
			context.lineTo(x, this.config.HEIGHT-this.config.TICKS_HEIGHT);
			context.stroke();
		}, this);


	},

	draw_battery: function(context, data){
		if (!data || data.length === 0) return;

		var yscale = d3.scale.linear()
							.domain([0, 100])
							.rangeRound([this.config.HEIGHT-this.config.TICKS_HEIGHT, 0]);

		var x;

		context.beginPath();
		context.fillStyle= "#A01517";
		context.strokeStyle = "#900507";
		context.moveTo(0, this.config.HEIGHT-this.config.TICKS_HEIGHT);

		context.moveTo(0, this.config.HEIGHT-this.config.TICKS_HEIGHT);
		_.each(data, function(elm, index){
			x = this._scaleX(elm.timestamp*1000);
			context.lineTo(x, yscale(elm.level));

			context.fillRect(x, yscale(elm.level), 2, 2);
		}, this);
		context.stroke();
	},

	draw_acce_force: function(context, data){
		if (!data || data.length === 0) return;
		var yscale = d3.scale.linear()
							.domain(this.device == "ios" ? [0, 3] : [8, 12])
							.rangeRound([this.config.HEIGHT-this.config.TICKS_HEIGHT, 0]);

		var x;

		context.beginPath();
		context.fillStyle= "#A01517";
		context.strokeStyle = "#900507";
		context.moveTo(0, this.config.HEIGHT-this.config.TICKS_HEIGHT);

		context.moveTo(0, this.config.HEIGHT-this.config.TICKS_HEIGHT);
		_.each(data, function(elm, index){
			x = this._scaleX(elm.timestamp*1000);
			context.lineTo(x, yscale(elm.force));

			context.fillRect(x, yscale(elm.force), 1, 1);
		}, this);
		context.stroke();
	},

	draw_data: function(context, data){
		context.fillStyle = "#fdfdfd";
		context.fillRect(0, 0, this.config.WIDTH, this.config.HEIGHT-this.config.TICKS_HEIGHT);

		this.draw_grid(context);

		switch(data){
			case "gps": this.draw_gps(context, this.gps_data); break;
			case "acce": this.draw_existance(context, this.acce_data); break;
			case "acce_force": this.draw_acce_force(context, this.acce_data); break;
			case "battery": this.draw_battery(context, this.battery_data); break;
		}

		this.draw_labels(context);
	},

	createCanvas: function(name){
		var elm = jQuery('<div class="raw_item"><span class="name">'+name+'</span></div>');
		var canvas = jQuery('<canvas/>');
		canvas.attr('width', this.config.WIDTH);
		canvas.attr('height', this.config.HEIGHT);
		elm.append(canvas);
		return elm;
	},

	render: function() {
		this.gps_container = this.createCanvas("GPS");
		this.draw_data(jQuery('canvas',this.gps_container)[0].getContext("2d"), "gps");

		this.acce_container = this.createCanvas("Accelerometer");
		this.draw_data(jQuery('canvas',this.acce_container)[0].getContext("2d"), "acce");

		this.battery_container = this.createCanvas("Battery Levels");
		this.draw_data(jQuery('canvas',this.battery_container)[0].getContext("2d"), "battery");

		this.acce_force_container = this.createCanvas("Force");
		this.draw_data(jQuery('canvas', this.acce_force_container)[0].getContext("2d"), "acce_force");

		this.container.html('');
		this.container.append(this.gps_container);
		this.container.append(this.acce_container);
		this.container.append(this.battery_container);
		this.container.append(this.acce_force_container);
	},

	unload: function(){
		this.container.html('');
	}
}
