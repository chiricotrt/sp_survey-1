# encoding: utf-8

class Notifier < ActionMailer::Base
  #default_url_options[:host] = "happymobility.sg"
  default :from     => "London Mobility Survey <londonfms@gmail.com>",
          :reply_to => "londonfms@gmail.com"

  def password_reset_instructions(user)
    msg = {
      :subject => "Password Reset Instructions",
      :to      => user.email
    }
    @content = {:edit_password_reset_url => edit_password_reset_url(user.perishable_token), :user => user.name}

    mail(msg)
  end

  def detectStopsCrashed(exception, user_id)
    msg = {
      :subject => "DetectStops Script Exception",
      :to      => "londonfms@gmail.com"
    }
    @exception = exception
    @user_id = user_id

    mail(msg)
  end

  def activation_instructions(user)
    msg = {
      :subject => "Activation Instructions",
      :to      => user.email
    }
    @content = {account_activation_url: activations_url(user.perishable_token), user: user.name}

    mail(msg)
  end

  def activation_confirmation(user)
    @root_url = root_url
    @user_name = user.name

    mail(:subject => "Activation Complete", :to => user.email)
  end

  def support_data(user,data)
    msg = {
      :subject => "Happy Mobility Survey: Move Issue from #{user.email}",
		  :to      => 'londonfms@gmail.com'
    }
		@values = data

    mail(msg)
  end

  def support_notification(user)
    mail(:subject => "London Mobility Survey: Message delivered to the team", :to => user.email)
  end

  def pre_register(data)
    msg = {
      :subject => "London Mobility Survey: Interested in participating",
		  :to      => 'londonfms@gmail.com'
    }
		@values = data

    mail(msg)
  end

  def household_activation(user, hhh)
    msg = {
      :subject => "Registration Confirmation",
      :to      => user.email
    }
    @content = {:user => user.name, :email => user.email, :household_head => hhh}
    attachments["iPhone_user_manual.pdf"] = {:mime_type => "application/pdf", :body => File.read("#{Rails.root}/public/docs/FMS_user_manual_iphone.pdf")}
    attachments["android_user_manual.pdf"] = {:mime_type => "application/pdf", :body => File.read("#{Rails.root}/public/docs/FMS_user_manual_android.pdf")}

    mail(msg)
  end

  def request_incentive(user, user_confirmed_data, no_days_data, no_days_valid)
    household_lta = HouseholdLtaId.where(household_id: user.household_id).first
    person_lta = UserLtaPerson.where(user_id: user.id)
    surveyid = "--"
    if ( (not household_lta.nil?) && (not person_lta.nil?))
      surveyid = "#{household_lta.postal_code}#{household_lta.lta_household}#{person_lta.lta_person}"
    end

    msg = {
      :subject => "Incentive Claim from #{user.email}",
      :to      => 'londonfms@gmail.com'
    }
    @content = {:household_id => user.household_id, :surveyid => surveyid, :user_confirmed_data => user_confirmed_data, :email => user.email, :user => user.name, :id => user.id, :no_stops_validated => no_days_valid, :no_stops => no_days_data}
    #body        :content => {:household_id => user.household_id, :user => user.name, :id => user.id, :no_stops_validated => no_days_valid, :no_stops => no_days_data}

    mail(msg)
  end

  def reminder_not_using(user)
    msg = {
      :subject => "London Mobility Survey: Following up on participation",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name}

    mail(msg)
  end

  def reminder_not_using_after_start_using(user, days_validated, days_with_data)
    msg = {
      :subject => "London Mobility Survey: Following up on participation",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name, :num_validated => days_validated, :num_days => days_with_data}

    mail(msg)
  end

  def reminder_not_validating(user)
    msg = {
      :subject => "London Mobility Survey: Participation & next steps",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name}

    mail(msg)
  end

  def reminder_not_validating_after_validated(user)
    msg = {
      :subject => "London Mobility Survey: Participation & next steps",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name}

    mail(msg)
  end


  def reminder_missing_days(user)
    msg = {
      :subject => "Gentle Reminder: Please collect more data",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name}

    mail(msg)
  end

  def reminder_missing_validation(user)
    msg = {
      :subject => "Gentle Reminder: Please validate more days",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name}

    mail(msg)
  end

  def welcoming_email(user)
    msg = {
      :subject => "Thank you for registering with the Happy Mobility Survey!",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name}

    mail(msg)
  end

  def mobile_app_off(user)
    msg = {
      :subject => "FMSurvey app is not running",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name}

    mail(msg)
  end


  def ready_for_exit(user)
    msg = {
      :subject => "Finished activity diary",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name}

    mail(msg)
  end

  def alarm_admin_lta(weird_cases)
    msg = {
      :subject => "FMSurvey LTA with weird number of stops",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:cases => weird_cases}

    mail(msg)
  end

  def alarm_battery_lta(weird_cases)
    msg = {
      :subject => "FMSurvey LTA with weird battery consumption",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:cases => weird_cases}

    mail(msg)
  end

  def session_reminder(user, starttime, endtime)
    msg = {
      :subject => "Help Session Reminder",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user, :starttime => starttime, :endtime => endtime}

    mail(msg)
  end

  def confirm_booking(booking)
    msg = {
      :subject => "Help Center Booking Registered",
      :to      => booking.slot.user.email,
      :content_type => "text/html"
    }
    @content = {:user => booking.user.name, :starttime => booking.slot.starttime, :endtime => booking.slot.endtime, :comments => booking.comment, :contact => booking.contact}

    mail(msg)
  end

  def cancel_booking(starttime, endtime, email)
    msg = {
      :subject => "Help Center Booking Canceled",
      :to      => email,
      :content_type => "text/html"
    }
    @content = {:starttime => starttime, :endtime => endtime}

    mail(msg)
  end

  def feedback_wanted(userid)
    user = User.find(userid)

    msg = {
      :subject => "FMSurvey LTA: Feedback needed",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name}

    mail(msg)
  end

  def finish_warning(user)
    msg = {
      :subject => "London Mobility Survey end date — 30 of September¬",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name}

    mail(msg)
  end

  def contact_user_email1(userid)
    user = User.find(userid)

    msg = {
      :subject => "FM Survey - Collect more data and get your $50 voucher",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name}

    mail(msg)
  end

  def contact_user_email2(userid)
    user = User.find(userid)

    msg = {
      :subject => "FM Survey - Collect more data and get your $50 voucher",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name}

    mail(msg)
  end

  def contact_user_email3(userid)
    user = User.find(userid)

    msg = {
      :subject => "FM Survey - Collect more data and get your $50 voucher",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name}

    mail(msg)
  end

  def contact_user_email4(userid)
    user = User.find(userid)

    msg = {
      :subject => "FM Survey - Collect more data and get your $50 voucher",
      :to      => user.email,
      :content_type => "text/html"
    }
    @content = {:user => user.name}

    mail(msg)
  end

  def user_locked_rawdata_error(user_id)
    msg = {
      :subject => "User: #{user_id} is now locked",
      :to      => "londonfms@gmail.com",
      :content_type => "text/html"
    }
    @content = {:user => user_id}

    mail(msg)
  end

  def user_report(report_path)
    msg = {
      :subject => "Internal user report",
      :to => ["sridhar.raman@gmail.com, melinda.matyas.13@ucl.ac.uk"],
      :content_type => "text/html"
    }
    @content = {:time => DateTime.now}
    attachments['specific_user_report.csv'] = {:mime_type => "application/csv", :body => File.read(report_path)}

    mail(msg)
  end

end
