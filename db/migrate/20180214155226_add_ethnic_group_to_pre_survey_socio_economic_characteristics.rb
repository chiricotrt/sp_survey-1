class AddEthnicGroupToPreSurveySocioEconomicCharacteristics < ActiveRecord::Migration
  def change
    add_column :pre_survey_socio_economic_characteristics, :ethnic_group, :integer
  end
end
