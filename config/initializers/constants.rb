#constants used in user model. related with properties values
NEVER_VALIDATED = 0
IGNORED_WELCOME_MSG = 1
IGNORED_SCHEDULE_MSG = 2
EMAIL_SENT = 3
SESSION_BOOKED = 4
SESSION_COMPLETED = 5

#user properties names
SCHEDULE_PROMPT = "schedule_prompt" #used to know if we should prompt the user about scheduling a session
SUGGESTED_DAY = "suggested_day" #used to know if the day is a suggested day for validation
DAY_TO_VALIDATE = "good_day_to_validate" #used to know if this day is good enough to validate

USER_LOCKED = "user_locked" #used for users which create raw data caused an error

#prompt states names
STATE0 = "welcome"
STATE1 = "welcome_validated"
STATE2 = "feedback"
STATE4 = "session_booked"

PROMPT_NEXT_VALUE = {STATE0 => 0, STATE1 => 1, STATE2 => 2, STATE4 => 4}

WARNING_PROMPT = 'warning_prompt'


#SPECIAL_USERS = [1,37,5,16,18,296,330]
SPECIAL_USERS = [1753]
FULL_DAY_USERS_DATE = "2013-09-16"

VALIDATED_STATES = {0 => "Nothing", 1 => "Activity", 2 => "Travel", 3 => "Both"}

HOST_SG = {
  lat: 1.359516, lon: 103.844748, location: 'Singapore'
}

HOSTS_LIST = [
  {host: 'happymobility.org', code: 'happy', description: 'Main server', primary: true}.merge(HOST_SG),
  {host: 'boston.fmsensing.com', code: 'boston', module: 'dummy', description: 'FMS in Boston for MBTA', lat: 42.3601, lon: -71.0589, location: 'Boston, USA'},
  {host: 'london.fmsensing.com', code: 'london', module: 'dummy', description: 'Innovation in Traveling Survey', lat: 51.5245592, lon: -0.1340401, location: 'London, UK'},
  {host: 'melbourne.fmsensing.com', code: 'melbourne', description: 'Victorian Future Mobility Sensing Project', lat: -37.7963, lon: 144.9614, location: "Melbourne, Victoria, Australia"},
  {host: 'delhi.fmsensing.com', code: 'delhi', module: 'dummy', description: 'FMS in Delhi', lat: 28.6139, lon: 77.2090, location: 'Delhi, India'},
  {host: 'fmsensing.com', code: 'fmsensing', description: 'Generic server'}.merge(HOST_SG),
  {host: "staging.happymobility.org", code: 'staging', description: 'Test server', development: true}.merge(HOST_SG)
]

# Stop detection related constants
MAX_WIFI_NUM = 5
MAX_GPS_ACCURACY = 1000 # Any GPS point with accuracy larger than this threshold is ignored
MAX_GSM_ACCURACY = 100 # Any GSM point with accuracy larger than this threshold will be ignored in speed estimation for mode detection
MIN_GSM_SS = 5 # Any GSM point with signal strength less than this threshold is ignored
MAX_ALT = 2000 # Use this temporarily to filter out impossible GPS points in SG
GPS_PROVIDER = 'GPS' # The constant to indicate the gps data.
NETWORK_PROVIDER = 'NETWORK' # The constant to indicate the GSM, WIFI data.
BIG_STOP = 15*60 # Minimum duration of a "big" stop that separates trips
BIG_START_STOP = 15*60 # Minimum duration of a "big" stop that separates trips
HAPPY_ONPHONE_ACTIVITY_QID = 293 # The question id of onphone happines activity questionnaire.
PAST_ACTIVITY_STOP_RADIUS = 0.100 # The radius to fetch activities for automated activity detection
MIN_MOTION_CONFIDENCE = 50 # Minimum confidence required to use motion data from phones.
HAPPY_ONPHONE_QID = 292

SHORT_TRIP_DURATION = 3*60 #Alternative mode detection procedure for trips shorter than 3 mins
SHORT_TRIP_DIST = 800  #Alternative mode detection procedure for trips shorter than 800 meters

MIN_REQ_POINTS=5 #Minimum GPS points required for feature calculation.

T_DUR_GPS = 60.000000 # Minimum duration of a stop in seconds for GPS points
L_ROAM_GPS_PHONE = 50.000000 # Max distance between points within a GPS stop for smart phones, in meters
L_ROAM_GPS_LOGGER = 50.000 #Max distance between points within a GPS stop for GPS loggers, in meters
T_DUR_GSM = 2*60.000000 # Minimum duration of a stop in seconds for GSM points?
L_ROAM_GSM = 100 # Max distance between points within a GSM stop, in meters
T_DUR_STEP_SIZE = 15 # Used in relation to function diameter() to reduce computation, only consider points whose time difference are larger than this step size
T_INTERVAL = 5*60 # Min interval between two stops. If interval less than this, merge the two stops
T_CHANGE_MODE = 10*60
DATABASE_TIME_ZONE="UTC" # Default time zone in the database in which the data is stored in.

MIN_MOTO_SPEED = 2.5000 #m/s
SPEED_ERROR_CONTROL = 85.00000
MIN_ERROR_SPEED = 40.00000000 #m/s
JUMP_RATIO = 5.0000 #The ratio of the distance between errorneous stop and the nearest next stop.
MIN_MRT_SPEED = 18.5000
MAX_MOTO_SPEED = 25.00000
MIN_AIR_SPEED = 120 #Minimum cruising speed for planes. Speed range -> 878–926 km/h = 243-257 m/s https://en.wikipedia.org/wiki/Cruise_(aeronautics)
MIN_AIR_DISTANCE = 50*1000 #m minimum distance required to be traveled for air mode. To avoid tagging small jumps with air mode.

ACCE_SAMPLE_INTERVAL = 10
ACCE_SAMPLE_LIMIT = 30 # instead of using ACCE_SAMPLE_INTERVAL and 5min of acce data everytime, maybe we should use a limit on the number of acce samples and spread them out evenly in the interval
SLOW_MOVE = 0.5
GPS_DATA_GAP = 10*60 # When the gap in GPS data is larger than GPS_DATA_GAP seconds, try to find GSM data to fill in
MODE_GPS_DATA_GAP = 30*60 # When the gap in GPS data is larger than MODE_GPS_DATA_GAP, do not perform mode detection.
MIN_STOP_DIST_SMALL = 50 # Minimum distance between two stops before and after a gap. If smaller than this, merge the stops.Use smaller threshold for round 1 to retain as many candidate stops for mode detection.
MIN_STOP_DIST_LARGE = 150 # Minimum distance between two stops before and after a gap. If smaller than this, merge the stops. Use larger threshold to remove noisy stops in round 2
GPS_PTS_MIN_DIST = 30 # For encoding traces
GPS_PTS_MAX_DIST = 100000 # For encoding traces  ZF-Changed to 100km so that we only have one segment per trip for most trips
GSM_POINTS_DIST = 1000 # Where is this number from?
MAX_GAP_TO_MERGE = 60*60*8 # Do not merge any stops if the gap between them is larger than 8 hours
MAX_WALK_DISTANCE = 2000 #meters - Maximum walking distance allowed for automatic recognition

MAX_STOP_ACCURACY = 250 #The maximum stop accuracy beyond which the location of stop cannot be trusted.
FREQUENT_PLACE_MAX_DISTANCE = 150 # Default max distance between frequent place and stops

GPS_FREQ_LOW = 40
HIGH_GPS_ACCURACY=100

#LOAD THE SOURCES INTO VARIABLES HERE
# tmp_source = Source.find_by_source_type("user_generated")
# USER_GENERATED = tmp_source.id unless tmp_source.nil?
# tmp_source = Source.find_by_source_type("user_changed")
# USER_CHANGED = tmp_source.id unless tmp_source.nil?
# tmp_source = Source.find_by_source_type("algorithm_generated")
# ALGORITHM_GENERATED = tmp_source.id unless tmp_source.nil?

